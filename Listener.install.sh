export cmdl="-o Apt::Get::Assume-Yes=true"

apt upgrade
apt update

echo '\n\n-----audio server-----\n\n'

apt $cmdl install pulseaudio
apt $cmdl install alsa
apt $cmdl install alsa-tools
apt $cmdl install sox
apt-get -y install libasound2-dev
modprobe snd-aloop
pulseaudio --system -D
sudo usermod -aG pulse,pulse-access $USER

echo '\n\n-----nodejs-----\n\n'

apt install curl
curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh
bash /tmp/nodesource_setup.sh
apt $cmdl install nodejs
apt $cmdl install npm
npm install -g npm

echo '\n\n-----server and essentials-----\n\n'

apt $cmdl install ffmpeg
apt $cmdl install python3
apt $cmdl install python3-pip
apt $cmdl install git
apt $cmdl install expect

git clone https://f2fdsfsdf:B%qtbVP5L_x76.j@gitlab.com/loltrymy321/listener.git
cd listener
npm i
cd ui
npm i
npm run build
cd ..
mkdir temp/recordings
mkdir temp/converted-recordings
sed -i -e 's/new Error(`write() failed: ${r}`)//g' node_modules/speaker/index.js

pip3 install telethon
pip3 install PySocks

echo '\n\n-----browser-----\n\n'

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome-stable_current_amd64.deb
apt $cmdl install libu2f-udev fonts-liberation
apt $cmdl install chromium-browser

echo '\n\n-----graphics-----\n\n'

apt install xfce4

echo "#%PAM-1.0
auth    requisite       pam_nologin.so
# auth  required        pam_succeed_if.so user != root quiet_success
@include common-auth
auth    optional        pam_gnome_keyring.so
@include common-account
# SELinux needs to be the first session rule. This ensures that any
# lingering context has been cleared. Without this it is possible
# that a module could execute code in the wrong domain.
session [success=ok ignore=ignore module_unknown=ignore default=bad]        pam_selinux.so close
session required        pam_loginuid.so
# SELinux needs to intervene at login time to ensure that the process
# starts in the proper default security context. Only sessions which are
# intended to run in the user's context should be run after this.
# pam_selinux.so changes the SELinux context of the used TTY and configures
# SELinux in order to transition to the user context with the next execve()
# call.
session [success=ok ignore=ignore module_unknown=ignore default=bad]        pam_selinux.so open
session optional        pam_keyinit.so force revoke
session required        pam_limits.so
session required        pam_env.so readenv=1
session required        pam_env.so readenv=1 user_readenv=1 envfile=/etc/default/locale
@include common-session
session optional        pam_gnome_keyring.so auto_start
@include common-password" > /etc/pam.d/gdm-password